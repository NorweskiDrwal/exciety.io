import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import { loginUser, registerUser, setToLogin, setLoading, setToRegister } from '../../store/actions';

import Login from '../../modules/Login/Login';
import Register from '../../modules/Register/Register';

class LandingContainer extends Component {
  constructor (props) {
    super (props);
    this.state = {
      email: '',
      password: '',
      showLogin: true,
      showRegister: false,
      error: {},
      showError: false,
    };
  }

  componentWillReceiveProps ({ error }) {
    if (this.props.error !== error && error !== null) {
        this.setState({
          error,
          showError: true,
        });
    }
  }

  render () {
    const {
      showLogin, showRegister,
      email, password, error, showError,
    } = this.state;
    return (
      <Fragment>
        { showLogin &&
            <Login
              showRegister={this.showRegisterOnClick}
              email={email}
              setEmail={this.setEmail}
              password={password}
              setPassword={this.setPassword}
              login={this.login}
              error={error}
              showError={showError}
              toLogin={this.props.toLogin}
            />
        }
        { showRegister &&
            <Register
              showLogin={this.showLoginOnClick}
              email={email}
              setEmail={this.setEmail}
              password={password}
              setPassword={this.setPassword}
              register={this.register}
              error={error}
              showError={showError}
              toRegister={this.props.toRegister}
            />
        }
      </Fragment>
    )
  }

  showLoginOnClick = () => this.setState({ showLogin: true, showRegister: false })
  showRegisterOnClick = () => this.setState({ showLogin: false, showRegister: true })

  setEmail = e => this.setState({ email: e.target.value });
  setPassword = e => this.setState({ password: e.target.value });

  login = () => {
    this.props.setToLogin(true);
    setTimeout(() => {
      this.props.setLoading(true);
      this.props.loginUser({ email: this.state.email, password: this.state.password });
      this.setState({ email: '', password: '' });
    }, 220);
  };

  register = () => {
    this.props.setToRegister(true);
    setTimeout(() => {
      this.props.setLoading(true);
      this.props.registerUser({ email: this.state.email, password: this.state.password });
      this.setState({ email: '', password: '' });
    }, 220);
  }
}

const mapStateToProps = state => ({
  error: state.userReducer.userError,
  toLogin: state.uiReducer.toLogin,
  toRegister: state.uiReducer.toRegister,
  isLoading: state.uiReducer.isLoading,
});

const mapDispatchToProps = dispatch => ({
  registerUser: credentials => dispatch(registerUser(credentials)),
  loginUser: credentials => dispatch(loginUser(credentials)),
  setToLogin: bool => dispatch(setToLogin(bool)),
  setToRegister: bool => dispatch(setToRegister(bool)),
  setLoading: bool => dispatch(setLoading(bool)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LandingContainer);
