import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import s, { keyframes } from 'styled-components';

import styles from '../../styles/variables.json';

import ProfileKnob from '../../modules/Profile/ProfileKnob';
import NavigationDrawer from '../../modules/Navigation/NavigationDrawer';
import ProfileButtonMenu from '../../modules/Profile/Buttons/ProfileButtonMenu';
import ProfileButtonExit from '../../modules/Profile/Buttons/ProfileButtonExit';
import ProfileButtonSearch from '../../modules/Profile/Buttons/ProfileButtonSearch';
import FavsDrawer from '../../modules/Favs/FavsDrawer';

class ProfileContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openKnobMenu: false,
      openMenuDrawer: true,
      openFavsDrawer: false,
      itemFavsName: '',
      itemMenuName: '',
    }
  }

  render () {
    return (
      <Fragment>
        <ProfileKnob
          menuList={this.menuList}
          handleOpenKnobMenu={this.handleOpenKnobMenu}
          handleCloseKnobMenu={this.handleCloseKnobMenu}
          openKnobMenu={this.state.openKnobMenu}
          handleOpenFavsDrawer={this.handleOpenFavsDrawer}
          itemFavsName={this.state.itemFavsName}
          handleGetItemName={this.handleGetItemName}
          itemMenuName={this.state.itemMenuName}
        />
        <NavigationDrawer
          menuList={this.menuList}
          openMenuDrawer={this.state.openMenuDrawer}
        />
        <FavsDrawer
          itemFavsName={this.state.itemFavsName}
          openFavsDrawer={this.state.openFavsDrawer}
          handleCloseFavsDrawer={this.handleCloseFavsDrawer}
        />
        <ProfileButtonMenu
          openMenuDrawer={this.state.openMenuDrawer}
          handleMenuDrawer={this.handleMenuDrawer}
        />
        <ProfileButtonExit />
        <ProfileButtonSearch />
      </Fragment>
    );
  }

  menuList = [
    {name: 'spots',       icon: 'fa fa-map-marker',    rot: '180'},
    {name: 'friends',     icon: 'fa fa-users',         rot: '225'},
    {name: 'chat',        icon: 'fa fa-comments',      rot: '270'},
    {name: 'trophies',    icon: 'fa fa-trophy',        rot: '315'},
    {name: 'ranks',       icon: 'fa fa-cubes',         rot: '135'},
    {name: 'check-in',    icon: 'fa fa-thumb-tack',    rot: '90'},
    {name: 'challenges',  icon: 'fa fa-handshake-o',   rot: '45'},
    {name: 'settings',    icon: 'fa fa-cog',           rot: '0'},
  ];

  handleOpenKnobMenu = () => this.setState({ openKnobMenu: true });
  handleCloseKnobMenu = () => this.setState({ openKnobMenu: false });
  handleMenuDrawer = () => this.setState({ openMenuDrawer: !this.state.openMenuDrawer });
  handleOpenFavsDrawer = name => {
    this.setState({ itemFavsName: name });
    this.setState({ openFavsDrawer: true });
  };
  handleCloseFavsDrawer = () => this.setState({ openFavsDrawer: false });
  handleGetItemName = name => this.setState({ itemMenuName: name });
}

export default connect(null, null)(ProfileContainer);
