import React from 'react';
import { Route, Switch } from 'react-router';

// Import modules/routes
import Landing from './pages/Landing/LandingContainer';
import Profile from './pages/Profile/ProfileContainer';

export default (
  <Switch>
    <Route exact path="/" component={Landing}/>
    <Route path="/profile" component={Profile}/>
    <Route path="/" component={null}/>
    <Route component={null} />
  </Switch>
);