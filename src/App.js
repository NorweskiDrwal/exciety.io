import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
// Styles
import 'bootstrap/dist/css/bootstrap.css';
// routes
import routes from './routes';
// components
import Alerts from './common/Alerts'; 
import Loader from './common/Loader';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Fragment>
            <Alerts />
            <Loader />
            { routes }
          </Fragment>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
