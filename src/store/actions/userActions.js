import { db, auth } from '../../configs/firebase';
import { push } from 'react-router-redux';

import { setToLogin, setLoading, setToRegister, addAlert } from './index';
import styles from '../../styles/variables.json';

export const getUserError = error => ({
	type: 'GET_USER_ERROR',
	payload: error,
});

export const getUserSuccess = user => ({
	type: 'GET_USER_SUCCESS',
	payload: user,
});

export const registerUser = credentials => {
  return function (dispatch) {
    dispatch(setLoading(true));
    auth
      .createUserWithEmailAndPassword(credentials.email, credentials.password)
        .then(user => {
          if (user) {
            const uid = user.user.uid;
            const userDoc = db.collection('users').doc(uid);
            userDoc.set({
              uid: uid,
              email: credentials.email,
            });
            dispatch(setLoading(false));
            dispatch(push('/setup'));
            dispatch(setToRegister(false));            
          }
        })
        .catch(error => {
          dispatch(setToRegister(false));
          dispatch(setLoading(false));
          dispatch(addAlert({
            color: styles.color.error,
            message: error.message,
          }));
        });
	}
}

export const loginUser = credentials => {
  return function (dispatch) {
    auth
      .signInWithEmailAndPassword(credentials.email, credentials.password)
        .then(() => dispatch(getUser()))
        .catch(error => {
          dispatch(setToLogin(false));
          dispatch(setLoading(false));
          dispatch(addAlert({
            color: styles.color.error,
            message: error.message,
          }));
        })
  }
}

export const getUser = type => {
  return function (dispatch) {
    auth.onAuthStateChanged((user) => {
        if (user) {
          const uid = user.uid;
          const userDoc = db.collection('users').doc(uid);
          userDoc.get()
            .then(doc => {
              if (doc.exists) { dispatch(getUserSuccess(doc.data())) }
              else { dispatch(addAlert({ message: "The user doesn't exist" })) }
            })
            .then(() => {
              dispatch(setLoading(false));
              dispatch(push('/profile'));
              dispatch(setToLogin(false));
              dispatch(setToRegister(false));
              dispatch(addAlert({
                color: styles.color.success, 
                message: "You are logged in",
              }));
              
            })
            .catch(error => {
              dispatch(addAlert(error));
            })
        }
      });
  }
}
