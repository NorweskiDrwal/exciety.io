export const setLoading = bool => ({
  type: 'SET_LOADING',
  payload: bool,
});

export const setToLogin = bool => ({
	type: 'SET_TO_LOGIN',
	payload: bool,
});

export const setToRegister = bool => ({
	type: 'SET_TO_REGISTER',
	payload: bool,
});
