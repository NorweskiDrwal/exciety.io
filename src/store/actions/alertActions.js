import createAlert from '../../utils/createAlert';

export const addAlert = (options = {}) => ({
	type: 'ADD_ALERT',
	payload: createAlert(options),
});

export const removeAlert = id => ({
	type: 'REMOVE_ALERT',
	payload: id,
});

