const alertReducer = (state = [], { type, payload }) => {
  switch (type) {
    case 'ADD_ALERT':
      return [payload, ...state];

    case 'REMOVE_ALERT':
      return state.filter(alert => alert.id !== payload);

    default:
      return state;
  }
};

export default alertReducer;
