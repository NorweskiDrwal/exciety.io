import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import userReducer from './userReducer';
import uiReducer from './uiReducer';
import alertReducer from './alertReducer';

// import your Module reducers here and combine them

const rootReducer = combineReducers({
	uiReducer,
	userReducer,
	alertReducer,
	router: routerReducer,
});

export default rootReducer;