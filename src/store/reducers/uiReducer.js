const initialState = {
  isLoading: false,
  toLogin: false,
  toRegister: false,
};

const uiReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_LOADING':
      return {
        ...state,
        isLoading: payload,
      };

    case 'SET_TO_LOGIN':
      return {
        ...state,
        toLogin: payload,
      };

    case 'SET_TO_REGISTER':
      return {
        ...state,
        toRegister: payload,
      };

    default:
      return state;
  }
};

export default uiReducer;
