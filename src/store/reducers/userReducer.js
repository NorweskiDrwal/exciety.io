const initialState = {
  userData: null,
  userError: null,
  userLoading: false,
};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'GET_USER_LOADING':
      return {
        ...state,
        userLoading: true,
      };

    case 'GET_USER_ERROR':
      return {
        ...state,
        userError: payload,
        userLoading: false,
      };

    case 'GET_USER_SUCCESS':
      return {
        ...state,
        userData: payload,
        userLoading: false,
      };

    default:
      return state;
  }
};

export default userReducer;
