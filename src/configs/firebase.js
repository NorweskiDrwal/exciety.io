import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// used when running locally (gatsby develop)
const config = {
  apiKey: "AIzaSyABrBHeGvgPkhjJ7ak_kZpwbBOMvbeRw9U",
  authDomain: "exciety-dev.firebaseapp.com",
  databaseURL: "https://exciety-dev.firebaseio.com",
  projectId: "exciety-dev",
  storageBucket: "exciety-dev.appspot.com",
  messagingSenderId: "27445231952",
};

firebase.initializeApp(config);

const db = firebase.firestore();
const auth = firebase.auth();

export {
  db,
  auth,
};
