import React, { Component } from 'react';
import s, { keyframes } from 'styled-components';

import styles from '../../styles/variables.json';

import { Form, Input, SubmitButton, SubmitButtonIcon, InputBox, InputTip } from '../../styles/common';

class Register extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {
      showLogin, email, setEmail, password, register,
      setPassword, error, showError, toRegister
    } = this.props;
    const emailOK = email !== '';
    const passOK = password.length >= 6;
    const disabled = !emailOK && !passOK;
    
    return (
      <FormWrap style={toRegister ? squeeze : null}>
        <FormInternals style={toRegister ? shrink : null}>
          <Title> Register now. The world of excieties awaits! </Title>
          <hr />
          <Input type="email" placeholder="email" value={email} onChange={setEmail} />
          <InputBox>
            <Input type="password" placeholder="password" value={password} onChange={setPassword} />
            { !passOK && password !== '' && <InputTip>Password should be at least 6 characters long!</InputTip> }
          </InputBox>
          <RegisterButton onClick={register} disabled={disabled} style={{ cursor: disabled ? 'not-allowed' : 'pointer' }}> Register </RegisterButton>
          <hr />
          <Title style={{ paddingTop: 0 }}> Or sign up with: </Title>
          <FacebookButton block>
            <SubmitButtonIcon className="fa fa-facebook-official" aria-hidden="true" />
            <IconName>Facebook</IconName>
          </FacebookButton>
          <Title style={{ fontSize: '0.8rem', marginTop: 20, marginBottom: '-10px' }}>
            Back to <Link onClick={showLogin}>login</Link>
          </Title>
        </FormInternals>
      </FormWrap>
    );
  }
}

const squeeze = {
  height: 150,
  width: 150,
  borderRadius: '50%',
};

const shrink = {
  transform: 'scale(0)',
}

const FormWrap = Form.extend`
  background: ${styles.color.purple};
  height: 480px;
`;

const FormInternals = s.div`
  transform: scale(1);
  transition: all .01s ease;
`;

const Title = s.p`
  color: ${styles.color.yellow};
  padding-top: 10xp;
`;

const RegisterButton = SubmitButton.extend`
  background: ${styles.color.lightgrey};
  color: ${styles.color.purple};
  :hover {
    background: ${styles.color.grey};
    color: ${styles.color.yellow};
  };
`;

const FacebookButton = SubmitButton.extend`
  background: #4A67AD;
  border: 0;
  color: ${styles.color.white};
`;

const IconName = s.p`
  margin-bottom: 0;
`;

const Link = s.span`
  cursor: pointer;
  text-decoration: underline;
`;

export default Register;
