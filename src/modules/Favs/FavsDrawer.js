import React from 'react';
import s, { keyframes } from 'styled-components';
import classNames from 'classnames/bind';

import styles from '../../styles/variables.json';
import { Centered } from '../../styles/common';

const FavsDrawer = ({
  openFavsDrawer, itemFavsName, handleCloseFavsDrawer,
}) => {
  return (
    <FavsDrawerWrap style={openFavsDrawer ? slideIn : null}>
      <FavsClose className="fa fa-chevron-circle-right" aria-hidden="true" onClick={handleCloseFavsDrawer} />
      <FavsDrawerTitle> { itemFavsName } </FavsDrawerTitle>
    </FavsDrawerWrap>
  )
}

const slideIn = {
  right: 0,
}

const FavsDrawerWrap = s.div`
  background: ${styles.color.white};
  border-left: 2px solid ${styles.color.red}; 
  bottom: 0;
  font-size: 1rem;
  font-family: ${styles.font.main};
  overflow-x: auto;
  position: absolute;
  right: -200px;
  top: 0;
  transition: all .3s ease;
  width: 200px;
  z-index: 95;
`;

const FavsClose = s.i`
  color: ${styles.color.white};
  left: 15px;
  position: absolute;
  top: 21px;
  transition: all 0.3s ease;
  :hover {
    cursor: pointer;
    transform: scale(1.5);
  }
`;

const FavsDrawerTitle = s.div`
  background: ${styles.color.red};
  color: ${styles.color.white};
  padding: 18px 0;
  text-align: center;
`;

export default FavsDrawer;
