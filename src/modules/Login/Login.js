import React, { Component, Fragment } from 'react';
import s, { keyframes } from 'styled-components';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import styles from '../../styles/variables.json';

import { Form, Input, SubmitButton, SubmitButtonIcon, InputBox, InputTip } from '../../styles/common';
class Login extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const {
      showRegister, email, setEmail, password, login,
      setPassword, error, showError, toLogin
    } = this.props;
    const emailOK = email !== '';
    const passOK = password.length >= 6;
    const disabled = !emailOK && !passOK;

    return (
      <FormWrap style={toLogin ? squeeze : null}>
        <FormInternals style={toLogin ? shrink : null}>
          <Input type="email" placeholder="email" value={email} onChange={setEmail} />
          <InputBox>
            <Input type="password" placeholder="password" value={password} onChange={setPassword} />
            { !passOK && password !== '' && <InputTip>Password should be at least 6 characters long!</InputTip> }
          </InputBox>
          <LoginButton onClick={login} disabled={disabled}> 
            Login
          </LoginButton>
          <hr />
          <Title style={{ paddingTop: 0 }}> Or sign in with: </Title>
          <FacebookButton block>
            <SubmitButtonIcon className="fa fa-facebook-official" aria-hidden="true" />
            <IconName>Facebook</IconName>
          </FacebookButton>
          <Title style={{ fontSize: '0.8rem', marginTop: 20, marginBottom: '-10px' }}>
            <Link onClick={showRegister}>Click here</Link> if you don't have an account
          </Title>
        </FormInternals>
      </FormWrap>
    );
  }
}

const squeeze = {
  height: 150,
  width: 150,
  borderRadius: '50%',
};

const shrink = {
  transform: 'scale(0)',
}

const FormWrap = Form.extend`
  height: 450px;
`;

const FormInternals = s.div`
  transform: scale(1);
  transition: all 0.01s ease;
`;

const Title = s.p`
  color: ${styles.color.yellow};
  padding-top: 10xp;
`;

const LoginButton = SubmitButton.extend`
  background: ${styles.color.lightgrey};
  color: ${styles.color.red};
  :hover {
    background: ${styles.color.grey};
    color: ${styles.color.yellow};
  };
`;

const FacebookButton = SubmitButton.extend`
  background: #4A67AD;
  border: 0;
  color: ${styles.color.white};
`;

const Link = s.span`
  cursor: pointer;
  text-decoration: underline;
`;

const IconName = s.p`
  margin-bottom: 0;
`;

export default Login;
