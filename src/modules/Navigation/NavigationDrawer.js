import React from 'react';
import s, { keyframes } from 'styled-components';
import classNames from 'classnames/bind';

import styles from '../../styles/variables.json';
import { Centered } from '../../styles/common';

import NavigationDrawerItem from './NavigationDrawerItem';

const NavigationDrawer = ({
  menuList, openMenuDrawer
}) => {
  return (
    <NavigationDrawerWrap style={openMenuDrawer ? slideIn : null}>
      <NavigationDrawerMenu>
        <NavigationDrawerTitle>Hi, Mateusz</NavigationDrawerTitle>
        {menuList !== null && menuList.map(item => (
          <NavigationDrawerItem 
            {...item} 
            menuList={menuList}
            key={item.name}
          />
        ))}
      </NavigationDrawerMenu>
    </NavigationDrawerWrap>
  )
}

const slideIn = {
  left: 0,
}

const slideInLeft = keyframes`
  0% { left: -200px }
  100% { left: 0 }
`; 

const NavigationDrawerWrap = s.div`
  animation: ${slideInLeft} .3s ease;
  background: ${styles.color.white};
  border-right: 2px solid ${styles.color.red};
  bottom: 0;
  overflow-x: auto;
  position: absolute;
  top: 0;
  transition: all .3s ease;
  left: -200px;
  width: 200px;
  z-index: 95;
`;

const NavigationDrawerMenu = s.ul`
  color: ${styles.color.red};
  font-family: ${styles.font.main};
  list-style: none;
  :first-child {
    background: ${styles.color.red};
    height: 60px;
    line-height: 63px;
    padding-left: 0 !important;
  }
`;

const NavigationDrawerTitle = s.div`
  color: ${styles.color.white};
  margin-left: 70px;
  font-size: 1rem;
`;

export default NavigationDrawer;
