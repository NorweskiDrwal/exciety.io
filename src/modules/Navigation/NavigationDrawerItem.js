import React from 'react';
import s, { keyframes } from 'styled-components';
import classNames from 'classnames/bind';

import styles from '../../styles/variables.json';
import { Centered } from '../../styles/common';

const NavigationDrawerItem = ({ name, icon }) => {
  return (
    <NavigationDrawerItemWrap>
      <NavigationDrawerItemIcon className={icon} aria-hidden="true" />
      <NavigationDrawerItemName>{ name }</NavigationDrawerItemName>
    </NavigationDrawerItemWrap>
  )
}

const NavigationDrawerItemWrap = s.li`
  font-size: 1rem;
  line-height: 20px;
  padding: 10px 0;
  position: relative;
  :hover:not(:first-child) {
    background: ${styles.color.lightgrey};
    cursor: pointer;
  }
`;

const NavigationDrawerItemIcon = s.i`
  position: absolute;
  top: 50%;
  left: 20px;
  transform: translateY(-50%);
  font-size: 1.2rem;
`;

const NavigationDrawerItemName = s.span`
  margin-left: 70px;
`;

export default NavigationDrawerItem;
