import React from 'react';
import s, { keyframes } from 'styled-components';
import classNames from 'classnames/bind';

import styles from '../../styles/variables.json';
import { Centered } from '../../styles/common';

import FavsDrawer from '../Favs/FavsDrawer';

const ProfileKnobMenuItem = ({
  name, icon, rot, openKnobMenu, handleOpenFavsDrawer, handleGetItemName
}) => {
  return (
    <MenuItemAxis style={rotate(rot)}>
      <MenuItem
        style={open(openKnobMenu)}
        onClick={() => handleOpenFavsDrawer(name)}
        onMouseOver={() => handleGetItemName(name)}
        onMouseOut={() => handleGetItemName('')}
      >
        <MenuItemIcon className={icon} aria-hidden="true" style={straigt(rot)} />
      </MenuItem>
    </MenuItemAxis>
  )
}

const rotate = rot => ({ transform: `rotate(${rot}deg)` });
const straigt = rot => ({ transform: `rotate(-${rot}deg)` });
const open = openKnobMenu => {
  const toTop = openKnobMenu ? '200px' : '0';
  return { top: `${toTop}` };
};

const MenuItemAxis = Centered.extend`
  height: 60px;
  width: 60px;
  transition: all .3s ease;
`;

const MenuItem = s.div`
  bottom: 0;
  left: 0;
  position: absolute;
  margin: auto;
  right: 0;
  top: 0;
  cursor: pointer;
  background: ${styles.color.red};
  height: 55px;
  width: 55px;
  border-radius: 50%;
  transition: all .3s ease;
  :hover {
    transform: scale(1.2);
  }
  :active { background: ${styles.color.pink} };
`;

const MenuItemIcon = s.i`
  bottom: 0;
  left: 0;
  position: absolute;
  margin: auto;
  right: 0;
  top: 0;
  font-size: 2rem;
  transition: all .15s ease;
  ::before {
    color: ${styles.color.white};
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  };
`;

export default ProfileKnobMenuItem;
