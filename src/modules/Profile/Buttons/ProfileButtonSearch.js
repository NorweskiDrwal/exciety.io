import React, { Component } from 'react';
import s, { keyframes } from 'styled-components';

import styles from '../../../styles/variables.json';
import { Button, Centered, ButtonIcon } from '../../../styles/common';

const ProfileButtonSearch = () => {
  return (
    <ButtonSearch>
      <ButtonSearchIcon className="fa fa-search" aria-hidden="true"></ButtonSearchIcon>
    </ButtonSearch>
  );
}

const ButtonSearch = Button.extend`
  border: 4px solid ${styles.color.yellow};
  background: ${styles.color.grey};
  top: 10px;
  right: 10px;
  transition: all .3s ease;
  z-index: 9999;
  :hover {
    background: ${styles.color.white};
    transform: scale(1.3)
  }
  :active {
    background: ${styles.color.grey};
    box-shadow: 0 0 0 0.2rem rgba(255, 193, 7, 0.5);
  }
`;

const ButtonSearchIcon = ButtonIcon.extend`
  color: ${styles.color.white};
  :hover { color: ${styles.color.grey} }
  :active {
    
    color: ${styles.color.white};
  }
  ::before { top: 45% }
`;

export default ProfileButtonSearch;
