import React from 'react';
import s, { keyframes } from 'styled-components';

import styles from '../../../styles/variables.json';
import { Button, Centered, ButtonIcon } from '../../../styles/common';

const ProfileButtonMenu = ({ openMenuDrawer, handleMenuDrawer }) => {
  return (
    <ButtonMenu>
      <ButtonMenuIcon className={openMenuDrawer ? "fa fa-times" : "fa fa-bars"} aria-hidden="true" onClick={handleMenuDrawer}></ButtonMenuIcon>
    </ButtonMenu>
  );
}

const ButtonMenu = Button.extend`
  background: ${styles.color.white};
  border: 4px solid ${styles.color.yellow};
  top: 10px;
  left: 10px;
  transition: all .3s ease;
  z-index: 9999;
  :hover {
    background: ${styles.color.grey};
    border-color: ${styles.color.yellow};
    transform: scale(1.3)
  }
  :active {
    background: ${styles.color.white};
    box-shadow: 0 0 0 0.2rem rgba(255, 193, 7, 0.5);
  }
}

}
`;

const ButtonMenuIcon = ButtonIcon.extend`
  color: ${styles.color.red};
  :hover { color: ${styles.color.white} }
  :active { color: ${styles.color.red} }
`;

export default ProfileButtonMenu;
