import React from 'react';
import s, { keyframes } from 'styled-components';

import styles from '../../../styles/variables.json';
import { Button, Centered, ButtonIcon } from '../../../styles/common';

const ProfileButtonExit = () => {
  return (
    <ButtonExit>
      <ButtonExitIcon className="fa fa-power-off" aria-hidden="true"></ButtonExitIcon>
    </ButtonExit>
  );
}

const ButtonExit = Button.extend`
  border: 4px solid ${styles.color.yellow};
  background: ${styles.color.red};
  bottom: 10px;
  left: 10px;
  transition: all .3s ease;
  z-index: 9999;
  :hover {
    background: #bd2130;
    border-color: ${styles.color.red};
    transform: scale(1.3)
  }
  :active {box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.5);}
`;

const ButtonExitIcon = ButtonIcon.extend`
  color: ${styles.color.white};
  :hover { color: ${styles.color.yellow} }
  :active {color: ${styles.color.white} }
`;

export default ProfileButtonExit;
