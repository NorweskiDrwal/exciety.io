import React, { Component, Fragment } from 'react';
import s, { keyframes } from 'styled-components';
import postcss from 'postcss';

import styles from '../../styles/variables.json';
import KnobMenuItem from './ProfileKnobMenuItem.js';
import { Centered } from '../../styles/common';

const ProfileKnobMenu = ({
  openKnobMenu, handleOpenFavsDrawer, menuList, handleGetItemName
}) => (
  <KnobMenu>
    {menuList !== null && menuList.map(item => (
      <KnobMenuItem 
        {...item}
        key={item.name}
        openKnobMenu={openKnobMenu}
        handleOpenFavsDrawer={handleOpenFavsDrawer}
        handleGetItemName={handleGetItemName}
      />
    ))}
  </KnobMenu>
);

const KnobMenu = Centered.extend`
  height: 200px;
  width: 200px;
`;

export default ProfileKnobMenu;
