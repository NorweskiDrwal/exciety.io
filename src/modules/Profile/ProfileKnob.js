import React, { Component, Fragment } from 'react';
import s, { keyframes } from 'styled-components';

import styles from '../../styles/variables.json';
import { Centered } from '../../styles/common';
import picture from '../../assets/mati.jpg';

// components
import ProfileKnobMenu from './ProfileKnobMenu';
import Backdrop from '../../common/Backdrop';

const ProfileKnob = ({
  openKnobMenu, handleOpenKnobMenu, handleCloseKnobMenu, menuList, handleOpenFavsDrawer, itemFavsName, handleGetItemName, itemMenuName
}) => (
  <Fragment>
    { openKnobMenu && <Backdrop backdrop={handleCloseKnobMenu} /> }
    <ProfileKnobOutside style={openKnobMenu ? openKnob : null} />
    <ProfileKnobMenu openKnobMenu={openKnobMenu} menuList={menuList} handleOpenFavsDrawer={handleOpenFavsDrawer} handleGetItemName={handleGetItemName} />
    <ProfileKnobPicture onMouseOver={handleOpenKnobMenu} src={picture}/>
    <ProfileKnobItemName style={itemMenuName ? showTitle : null}>{ itemMenuName }</ProfileKnobItemName>
  </Fragment>
);

const ProfileKnobOutside = Centered.extend`
  background: ${styles.color.red};
  border-radius: 50%;
  height: 150px;
  transition: all .3s ease;
  width: 150px;
`;

const openKnob = {
  background: 'transparent',
  transform: 'scale(1.8)',
}

const pop = keyframes`
  0% { transform: scale(0) }
  70% { transform: scale(1.2) }
  100% { transform: scale(1) }
`; 

const ProfileKnobPicture = s.img`
  background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3));
  bottom: 0;
  left: 0;
  margin: auto;
  position: absolute;
  right: 0;
  top: 0;
  height: 130px;
  width: 130px;
  border-radius: 50%;
  animation: ${pop} .5s ease;
`;

const ProfileKnobItemName = Centered.extend`
  background: ${styles.color.red};
  color: ${styles.color.white};
  font-family: ${styles.font.main};
  border-radius: 8px;
  font-size: 1.2rem;
  height: 30px;
  line-height: 36px;
  text-align: center;
  overflow: hidden;
  word-break: unset;
  transition: all 0.2s ease-out;
  width: 0px;
  z-index: 1000;
`;

const showTitle = {
  width: 134,
  zIndex: '1',
}

export default ProfileKnob;
