import React, { Component } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import s, { keyframes } from 'styled-components';

import { removeAlert } from '../store/actions'
import styles from '../styles/variables.json';

// import Alert from './Alert';

class Alerts extends Component {
  render () {
    const {
      alerts, actions,
    } = this.props;
    return (
      <AlertList>
        { 
          alerts !== undefined ? alerts.map(alert => {
            const { id } = alert;
            return (
              <Alert
                key={id}
                onClick={() => this.removeAlert(id)}
                autoRemoveAlert={this.autoRemoveAlert(id)}
                style={{ background: alert.color }}
              >
                { alert.message }
              </Alert>
            );
          }) : null
        }
      </AlertList>
    );
  }

  removeAlert = id => this.props.removeAlert(id);
  autoRemoveAlert = id => setTimeout(() => {this.props.removeAlert(id)}, 6000);

};

// AlertsList.propTypes = {
//   actions: PropTypes.shape({
//     removeAlert: PropTypes.func.isRequired
//   }).isRequired,
//   alerts: PropTypes.arrayOf(PropTypes.object).isRequired
// };

const slideUp = keyframes`
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
`;

const AlertList = s.div`
  bottom: 12px;
  color: ${styles.color.white};
  right: 12px;
  font-size: .75rem;  
  padding: 5px;
  position: absolute;
  overflow: hidden;
  z-index: 9999;
  max-height: calc(100vh - 10px);
  text-align: right;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const Alert = s.div`
  box-shadow: 3px 3px 5px rgba(0,0,0,0.5);
  font-family: 'Arial';
  font-family: ${styles.font.main};
  display: flex;
  cursor: pointer;
  text-transform: uppercase;
  align-items: center;
  padding: 7px 20px 3px;
  white-space: pre-line;
  min-height: 40px;
  border-radius: .2rem;
  animation: ${slideUp} .4s both;
  :not(:last-child) {
    margin: 0 0 12px;
  }
`;

const mapStateToProps = state => ({
  alerts: state.alertReducer,
});

const mapDispatchToProps = dispatch => ({
  removeAlert: id => dispatch(removeAlert(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Alerts);

