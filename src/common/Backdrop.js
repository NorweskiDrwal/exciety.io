import React from 'react';

const Backdrop = ({backdrop}) => ( <div style={style} onClick={backdrop} /> );

const style = {
  position: 'absolute',
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  height: '100vh',
  width: '100vw',
}

export default Backdrop;
