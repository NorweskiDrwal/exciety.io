import React, { Component } from 'react';
import { connect } from 'react-redux';
import s, { keyframes } from 'styled-components';

import styles from '../styles/variables.json';

class Loader extends Component {
  render () {
    return (
      this.props.isLoading &&
        <Ring1 >
          <Ring2>
            <Ring3>
              <Ring4>
                <Ring5 />
              </Ring4>
            </Ring3>
          </Ring2>
        </Ring1>
    );
  }
} 

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const Wrapper = s.div`
  border: 10px solid transparent;
  border-radius: 50%;
  height: 150px;
  position: absolute;
  opacity: 0.95;
  width: 150px;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  z-index: 1000;
`;

const Ring1 = Wrapper.extend`
  animation: ${spin} 1s linear infinite;
  border-top: 10px solid ${styles.color.yellow};
`;

const Ring2 = Wrapper.extend`
  animation: ${spin} 2s linear infinite;
  border-right: 10px solid ${styles.color.purple};
  left: -10px;
`;

const Ring3 = Wrapper.extend`
  animation: ${spin} 3s linear infinite;
  border-left: 10px solid ${styles.color.white};
  left: -10px;
`;

const Ring4 = Wrapper.extend`
  animation: ${spin} 10s linear infinite;
  border-bottom: 10px solid ${styles.color.pink};
  left: -10px;
`;

const Ring5 = Wrapper.extend`
  animation: ${spin} 15s linear infinite;
  border-top: 10px solid ${styles.color.blue};
  left: -10px;
`;

const mapStateToProps = state => ({
  isLoading: state.uiReducer.isLoading,
});

export default connect(mapStateToProps, null)(Loader);
