import s from 'styled-components';
import styles from '../styles/variables.json';

export const Centered = s.div`
  bottom: 0;
  left: 0;
  margin: auto;
  position: absolute;
  right: 0;
  top: 0;
`;

export const Form = Centered.extend`
  background: ${styles.color.red};
  border: 10px solid ${styles.color.yellow};
  border-radius: 6rem;
  font-family: ${styles.font.main};
  height: fit-content;
  overflow: hidden;
  padding: 40px 60px;
  text-align: center;
  transition: all .2s linear;
  width: 450px;
  height: 480px;
`;

export const Input = s.input`
  background: #fff;
  background-clip: padding-box;
  border: 0;
  border-radius: 0.5rem;
  color: #495057;
  display: block;
  font-size: 1rem;
  line-height: 1.5;
  margin: 25px auto;
  padding: 0.375rem 0.75rem;
  width: 100%;
  :focus {
    outline: none;
  }
`;

export const SubmitButton = s.button`
  border: 0;
  border-radius: 0.5rem;
  cursor: pointer;
  margin-bottom: 10px;
  padding-bottom: 5px;
  padding-top: 7px;
  position: relative;
  text-align: center;
  width: 100%;
  :focus {
    outline: none;
  }
`;

export const SubmitButtonIcon = s.i`
  font-size: 30px;
  position: absolute;
  top: 3px;
  left: 5px;
`;

export const InputBox = s.div`position: relative`;

export const InputTip = s.span`
  color: ${styles.color.white};
  position: absolute;
  font-size: .6rem;
  width: fit-content;
  left: 50%;
  transform: translateX(-50%);
  text-transform: uppercase;
  top: 40px;
  z-index: 10;
`;

export const Button = s.div`
  border-radius: 50%;
  cursor: pointer;
  height: 40px;
  position: absolute;
  width: 40px;
`;

export const ButtonIcon = s.i`
  height: 40px;
  width: 40px;
  ::before {
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    font-size: 1.4rem;
  }
`;